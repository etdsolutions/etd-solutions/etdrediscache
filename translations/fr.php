<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{etdrediscache}prestashop>etdrediscache_e4b5b201f1062ddd02c4db8d5f779f5f'] = 'Votre configuration a été sauvegardée';
$_MODULE['<{etdrediscache}prestashop>etdrediscache_5272c9791a230181b34f682952776e42'] = 'Une erreur est survenur lors de la sauvegarde de votre configuration';
$_MODULE['<{etdrediscache}prestashop>etdrediscache_dd96c1909379ea802e5cc38dbed7d023'] = 'Vous devez configurer ce module';
$_MODULE['<{etdrediscache}prestashop>configure_c1d1fa75a13bd822e5b61058e8619b29'] = 'Configuration du cache Redis';
$_MODULE['<{etdrediscache}prestashop>configure_f38bfdca8aab8616fad2cba519946fb0'] = 'Merci de renseigner les informations concernant votre serveur';
$_MODULE['<{etdrediscache}prestashop>configure_505de52d5ae227b6e8acb02dce2a0c4d'] = 'Serveur :';
$_MODULE['<{etdrediscache}prestashop>configure_64ea53ab0a03507af104853e03c2db28'] = 'Port :';
$_MODULE['<{etdrediscache}prestashop>configure_d493374e2a832c1dbfc8b68c6faacf20'] = 'Timeout :';
$_MODULE['<{etdrediscache}prestashop>configure_33ae9939aa3e5de204205172b8a2d7ed'] = 'Base de donnée :';
$_MODULE['<{etdrediscache}prestashop>configure_30669dfa8b13460752fd553d3c02aa5c'] = 'Durée de vie par défaut :';
$_MODULE['<{etdrediscache}prestashop>configure_c9cc8cce247e49bae79f15173ce97354'] = 'Enregistrer';
$_MODULE['<{etdrediscache}prestashop>configure_86f2b5fa8f5efd9f37b4c5dc2ce0ecf8'] = 'NB : merci de contrôler que votre serveur Redis est correctement installé';
$_MODULE['<{etdrediscache}prestashop>configure_ca3e5ce1b1937903fb8291c7cc1ecc92'] = '(site Redis)';
