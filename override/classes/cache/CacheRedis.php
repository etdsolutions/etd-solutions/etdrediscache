<?php

/**
 * This class require Redis server and ETD Redis Cache module installed
 *
 */
class CacheRedis extends Cache
{
    /**
     * @var Redis
     */
    protected $redis;

    /**
     * @var bool Connection status
     */
    protected $is_connected = false;

    /**
     * @var int TTL par défaut
     */
    protected $default_ttl = 3600;

    /**
     * @var string Version of the Redis server
     */
    private $server_version;

    public function __construct()
    {

        $this->connect();
        if ($this->is_connected) {

            $this->redis->setOption(Redis::OPT_PREFIX, _DB_PREFIX_);

            if (defined('Redis::SERIALIZER_IGBINARY')) {
                $this->redis->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_IGBINARY);
            } else if (defined('Redis::SERIALIZER_MSGPACK')) {
                $this->redis->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_MSGPACK);
            } else if (defined('Redis::SERIALIZER_JSON')) {
                $this->redis->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_JSON);
            } else if (defined('Redis::SERIALIZER_PHP')) {
                $this->redis->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_PHP);
            } else {
                $this->redis->setOption(Redis::OPT_SERIALIZER, Redis::SERIALIZER_NONE);
            }

        }
    }

    public function __destruct()
    {
        $this->close();
    }

    /**
     * Connect to redis server
     */
    public function connect()
    {
        if (class_exists('Redis') && extension_loaded('redis')) {
            $this->redis = new Redis();
        } else {
            return;
        }

        $server = $this->getRedisServer();

        if ($server === false) {
            return;
        }

        $this->default_ttl = $server["ttl"];
        $this->is_connected = $this->redis->connect($server["host"], $server["port"], $server["timeout"]);
        if ($this->is_connected) {
            if (!empty($server["password"])) {
                if (!$this->redis->auth($server["password"])) {
                    $this->redis->close();
                    $this->is_connected = false;
                    return;
                }
            }
            $this->redis->select($server["db"]);
        }

    }

    /**
     * Cache a data
     *
     * @param string $key
     * @param mixed $value
     * @param int|array $options
     * @return bool
     */
    protected function _set($key, $value, $options = null)
    {
        if (!$this->is_connected) {
            return false;
        }

        return $this->redis->set($key, $value, $options);
    }

    /**
     * Retrieve a cached data by key
     *
     * @param string $key
     * @return mixed
     */
    protected function _get($key)
    {
        if (!$this->is_connected) {
            return false;
        }

        return $this->redis->get($key);
    }

    /**
     * Check if a data is cached by key
     *
     * @param string $key
     * @return bool
     */
    protected function _exists($key)
    {
        if (!$this->is_connected) {
            return false;
        }

        return (int)$this->redis->exists($key) > 0;
    }

    /**
     * Delete a data from the cache by key
     *
     * @param string $key
     * @return bool
     */
    protected function _delete($key)
    {
        if (!$this->is_connected) {
            return false;
        }

        return version_compare($this->getServerVersion(), "4.0.0", ">=") ? $this->redis->unlink($key) : $this->redis->del($key);
    }

    /**
     * Write keys index
     */
    protected function _writeKeys()
    {
        if (!$this->is_connected) {
            return false;
        }

        return true;
    }

    /**
     * @see Cache::flush()
     */
    public function flush()
    {
        if (!$this->is_connected) {
            return false;
        }

        if (version_compare($this->getServerVersion(), "4.0.0", ">=")) {
            return $this->redis->flushDb(true);
        }

        return $this->redis->flushDb();
    }

    /**
     * Store a data in cache
     *
     * @param string $key
     * @param mixed $value
     * @param int|array $options
     * @return bool
     */
    public function set($key, $value, $options = null)
    {

        if (!isset($options)) {
            $options = $this->default_ttl;
        }

        return $this->_set($key, $value, $options);
    }

    /**
     * Retrieve a data from cache
     *
     * @param string $key
     * @return mixed
     */
    public function get($key)
    {
        return $this->_get($key);
    }

    /**
     * Check if a data is cached
     *
     * @param string $key
     * @return bool
     */
    public function exists($key)
    {
        return $this->_exists($key);
    }

    /**
     * Delete one or several data from cache (* joker can be used, but avoid it !)
     *    E.g.: delete('*'); delete('my_prefix_*'); delete('my_key_name');
     *
     * @param string $key
     * @return bool
     */
    public function delete($key)
    {
        if ($key == '*') {
            return $this->flush();
        } elseif (strpos($key, '*') === false) {
            return $this->_delete($key);
        } else {
            $pattern = str_replace('\\*', '.*', preg_quote($key));
            $keys = $this->redis->keys("*");
            foreach ($keys as $key => $data) {
                if (preg_match('#^' . $pattern . '$#', $key)) {
                    if (!$this->_delete($key)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Returns version of the Redis server
     *
     * @return bool
     */
    public function getServerVersion()
    {

        if (!$this->is_connected) {
            return false;
        }

        if (isset($this->redis_version)) {
            return $this->redis_version;
        }

        $info = $this->redis->info("SERVER");

        if (!is_array($info) || !isset($info["redis_version"])) {
            return false;
        }

        $this->redis_version = $info["redis_version"];
        return $this->redis_version;

    }

    /**
     * Close connection to redis server
     *
     * @return bool
     */
    protected function close()
    {
        if (!$this->is_connected) {
            return false;
        }

        return $this->redis->close();
    }

    protected function getRedisServer()
    {

        $params = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT name, value FROM ' . _DB_PREFIX_ . 'configuration WHERE name IN ("ETD_REDIS_SERVER", "ETD_REDIS_PORT", "ETD_REDIS_TIMEOUT", "ETD_REDIS_DB", "ETD_REDIS_TTL", "ETD_REDIS_PASSWORD")', true, false);
        $data = [];
        foreach ($params as $key => $val) {
            $data[$val['name']] = $val['value'];
        }

        if (isset($data["ETD_REDIS_SERVER"]) && !empty($data["ETD_REDIS_SERVER"])) {
            return [
                "host" => $data["ETD_REDIS_SERVER"],
                "port" => empty($data["ETD_REDIS_PORT"]) ? 6379 : (int)$data["ETD_REDIS_PORT"],
                "timeout" => empty($data["ETD_REDIS_TIMEOUT"]) ? 0.0 : (float)$data["ETD_REDIS_TIMEOUT"],
                "db" => empty($data["ETD_REDIS_DB"]) ? 0 : (int)$data["ETD_REDIS_DB"],
                "ttl" => empty($data["ETD_REDIS_TTL"]) ? 3600 : (int)$data["ETD_REDIS_TTL"],
                "password" => $data["ETD_REDIS_PASSWORD"]
            ];
        }

        return false;

    }
}
