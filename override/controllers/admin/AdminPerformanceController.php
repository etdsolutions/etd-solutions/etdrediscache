<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminPerformanceController extends AdminPerformanceControllerCore
{
    public function initFieldsetCaching()
    {
        parent::initFieldsetCaching();

        $warning_redis = ' ' . $this->l('(you must install the [a]Redis extension[/a])');
        $warning_redis = str_replace('[a]', '<a href="https://github.com/phpredis/phpredis/blob/develop/INSTALL.markdown" target="_blank">', $warning_redis);
        $warning_redis = str_replace('[/a]', '</a>', $warning_redis);

        $this->fields_form[6]['form']['input'][2]['values'][] = [
            'id' => 'CacheRedis',
            'value' => 'CacheRedis',
            'label' => $this->l('Redis') . (extension_loaded('redis') ? '' : $warning_redis)
        ];

    }
}
