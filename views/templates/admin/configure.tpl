{$message}

<fieldset>
    <legend>{l s='Settings Redis Cache' mod='etdrediscache'}</legend>
    <form method="post">
        <p class="success">{l s='Please fill in the details about your redis server' mod='etdrediscache'}</p>
        <p>
            <label for="ETD_REDIS_SERVER">{l s='Server:' mod='etdrediscache'}</label>
            <input id="ETD_REDIS_SERVER" name="ETD_REDIS_SERVER" type="text" value="{$ETD_REDIS_SERVER}">
        </p>
        <p>
            <label for="ETD_REDIS_PORT">{l s='Port:' mod='etdrediscache'}</label>
            <input id="ETD_REDIS_PORT" name="ETD_REDIS_PORT" type="text" value="{$ETD_REDIS_PORT}">
        </p>
        <p>
            <label for="ETD_REDIS_TIMEOUT">{l s='Timeout:' mod='etdrediscache'}</label>
            <input id="ETD_REDIS_TIMEOUT" name="ETD_REDIS_TIMEOUT" type="text" value="{$ETD_REDIS_TIMEOUT}">
        </p>
        <p>
            <label for="ETD_REDIS_DB">{l s='DB:' mod='etdrediscache'}</label>
            <input id="ETD_REDIS_DB" name="ETD_REDIS_DB" type="text" value="{$ETD_REDIS_DB}">
        </p>
        <p>
            <label for="ETD_REDIS_TTL">{l s='Default TTL:' mod='etdrediscache'}</label>
            <input id="ETD_REDIS_TTL" name="ETD_REDIS_TTL" type="text" value="{$ETD_REDIS_TTL}">
        </p>
        <p>
            <label for="ETD_REDIS_PASSWORD">{l s='Auth Password:' mod='etdrediscache'}</label>
            <input id="ETD_REDIS_PASSWORD" name="ETD_REDIS_PASSWORD" type="text" value="{$ETD_REDIS_PASSWORD}">
        </p>
        <p>
            <label>&nbsp;</label>
            <button id="submit_conf" name="submit_conf" type="submit" class="button">{l s='Save' mod='etdrediscache'}</button>
        </p>
        <p>
            {l s='NB : Please check if Redis server is correctly installed on your server' mod='etdrediscache'} <a href="http://redis.io/download" target="_blank"/> {l s='( Redis Server website ) ' mod='etdrediscache'}</a>
        </p>
    </form>
</fieldset>