<?php
/*
 * ETD Solutions - Editeur de commandes en massse
 *
 * @version		1.0
 * @copyright	Copyright (C) 2014 ETD Solutions, SARL Etudoo. Tous droits réservés.
 * @license		http://www.etd-solutions.com/licence
 * @author		ETD Solutions http://www.etd-solutions.com
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class EtdRedisCache extends Module
{

    /**
     * @var array $config
     */
    public $config;

    public function __construct()
    {

        $this->name = 'etdrediscache';
        $this->tab = 'front_office_features';
        $this->version = '0.0.3';
        $this->author = 'ETD Solutions';
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);

        parent::__construct();

        $this->checkConf();

        $this->displayName = 'ETD Solutions : Redis Cache';
        $this->description = 'Use Redis as cache server to give best performance to your shop';

    }

    public function install()
    {

        return parent::install() && $this->installConf();
    }

    public function uninstall()
    {

        return parent::uninstall() && $this->uninstallConf();
    }

    public function getContent()
    {

        $message = '';

        if (Tools::isSubmit('submit_conf')) {
            $message = $this->saveConf();
        }

        $this->context->smarty->assign([
            'message' => $message,
            'ETD_REDIS_SERVER' => Configuration::get('ETD_REDIS_SERVER'),
            'ETD_REDIS_PORT' => Configuration::get('ETD_REDIS_PORT'),
            'ETD_REDIS_TIMEOUT' => Configuration::get('ETD_REDIS_TIMEOUT'),
            'ETD_REDIS_DB' => Configuration::get('ETD_REDIS_DB'),
            'ETD_REDIS_TTL' => Configuration::get('ETD_REDIS_TTL'),
            'ETD_REDIS_PASSWORD' => Configuration::get('ETD_REDIS_PASSWORD'),
        ]);

        return $this->display(__FILE__, 'views/templates/admin/configure.tpl');

    }

    protected function installConf()
    {

        return Configuration::updateValue('ETD_REDIS_SERVER', '')
            && Configuration::updateValue('ETD_REDIS_PORT', '6379')
            && Configuration::updateValue('ETD_REDIS_TIMEOUT', '0')
            && Configuration::updateValue('ETD_REDIS_TTL', '3600')
            && Configuration::updateValue('ETD_REDIS_DB', '0')
            && Configuration::updateValue('ETD_REDIS_PASSWORD', '');
    }

    protected function saveConf()
    {

        $message = '';
        if (
            Configuration::updateValue('ETD_REDIS_SERVER', Tools::getValue('ETD_REDIS_SERVER'))
            && Configuration::updateValue('ETD_REDIS_PORT', Tools::getValue('ETD_REDIS_PORT'))
            && Configuration::updateValue('ETD_REDIS_TIMEOUT', Tools::getValue('ETD_REDIS_TIMEOUT'))
            && Configuration::updateValue('ETD_REDIS_DB', Tools::getValue('ETD_REDIS_DB'))
            && Configuration::updateValue('ETD_REDIS_TTL', Tools::getValue('ETD_REDIS_TTL'))
            && Configuration::updateValue('ETD_REDIS_PASSWORD', Tools::getValue('ETD_REDIS_PASSWORD'))
        ) {
            $message = $this->displayConfirmation($this->l('Your settings have been saved'));
        } else {
            $message = $this->displayError($this->l('There was an error while saving your settings'));
        }
        return $message;
    }

    protected function uninstallConf()
    {

        return Configuration::deleteByName('ETD_REDIS_SERVER')
            && Configuration::deleteByName('ETD_REDIS_PORT')
            && Configuration::deleteByName('ETD_REDIS_TIMEOUT')
            && Configuration::deleteByName('ETD_REDIS_DB')
            && Configuration::deleteByName('ETD_REDIS_TTL')
            && Configuration::deleteByName('ETD_REDIS_PASSWORD');
    }

    protected function checkConf()
    {
        $check = Configuration::get('ETD_REDIS_SERVER');

        if ($check === false || $check === '') {
            $this->warning = $this->l('You need to configure this module.');
        }
    }
}